#!/bin/bash

################
### Description:  The Search endpoint returns information about the projects,
###     repositories and helm charts offered at public status or related to
###     the current logged in user.  The response includes the project,
###     repository list and charts in a proper display order.
################

################
### TO DO:  Maybe update script to be able to take a search term for artifacts
###     or users (using the users/search method).
################

REGISTRY="registry.dev.internal"            #  Harbor server
SEARCHSTRING=()                             # Initialize array
APIUSER=${USER}                             # User to authenticate with
outputcmd="jq"                              # Default output to JSON

# Help text
usage() {
  echo "USAGE: $0 searchterm1 [searchterm2] [...] [OPTIONS]"
  echo "OPTIONS:"
  echo "  [-u|--user]         Override default user"
  echo "  [-r|--registry]     Override default registry"
  echo "  [-j|--json]         Display output in JSON (default)"
  echo "  [-y|--yaml]         Display output in YAML"
  echo "  [-h|--help]         Usage message"
  exit 0
}

# Parse arguments
while [[ $# -gt 0 ]]; do
  case $1 in
    --json|-j)
      outputcmd="jq"
      shift
      ;;
    --yaml|-y)
      outputcmd="yq r -PC -"
      shift
      ;;
    --user|-u)
      APIUSER="$2"
      shift 2
      ;;
    --registry|-r)
      REGISTRY="$2"
      shift 2
      ;;
    --help|-h)
      usage
      ;;
    *) SEARCHSTRING+=("$1")
      shift
      ;;
  esac
done

# Request API credentials
read -s -p "Enter password for ${APIUSER}: " APIPASSWORD
credentials=$(echo -n "${APIUSER}:${APIPASSWORD}" | base64)

# Exit if no search terms were provided
if [ ${#SEARCHSTRING[@]} -eq 0 ]; then echo "Search is empty... exiting." && exit 0; fi

# Get results for each word passed in
for word in ${SEARCHSTRING[@]}; do
  echo
  echo "----------------------------------------"
  echo "Search results for ${word}"
  echo "----------------------------------------"

  # Get raw JSON results
  response=$(curl -s -X "GET" \
    "https://${REGISTRY}/api/v2.0/search?q=${word}" \
    -H "accept: application/json" \
    -H "authorization: Basic ${credentials}") 
  # Format output based on arguments received
  echo $response | $outputcmd

  # If the results have a URL that we can download, display it
  if jq -e '.. | objects | select(has("urls"))' <<< "$response" > /dev/null ; then
    echo "----------------------------------------"
    echo "Commands to download Helm Charts for: ${word}"
    echo "----------------------------------------"
    # Extract URLs and their basenames using jq
    jq -r --arg REGISTRY "$REGISTRY" --arg credentials "$credentials" '
      .. | objects | select(has("urls")) | .urls[] |
      "curl -L -H \"Authorization: Basic \($credentials)\" -o $(basename \(.)) https://\($REGISTRY)\/chartrepo\/\(.)"
      ' <<< "$response"
  fi
done
