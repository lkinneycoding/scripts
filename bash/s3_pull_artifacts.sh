#!/bin/bash

### Description: Download objects out of a given S3 bucket and gzip them to be further distributed

BUCKET_NAME="mybucket"                           # Default bucket
S3_BUCKET_URL="s3://${BUCKET_NAME}"              # URL to bucket
TARGET_FOLDER="${HOME}/s3-downloads"             # Default local destination folder
TAGS=()                                          # All arguments are the version number/folder name to copy

# Help text
usage() {
  echo "USAGE: $0 searchterm1 [searchterm2] [...] [OPTIONS]"
  echo "OPTIONS:"
  echo "  [-b|--bucket]         Name of S3 bucket"
  echo "  [-d|--destination]    Local destination folder"
  echo "  [-h|--help]           Usage message"
  exit 0
}

# Process command line arguments
while [[ $# -gt 0 ]]; do
  case $1 in 
    # Get the name of the bucket
    --bucket|-b)
      BUCKET_NAME="$2"
      S3_BUCKET_URL="s3://${BUCKET_NAME}"
      shift 2
      ;;
    --destination|-d)
      TARGET_FOLDER="$2"
      shift 2
      ;;
    --help|-h)
      usage
      ;;
    # Add the remaining arguments to the array to be iterated through
    *) TAGS+=("$1")
      shift
      ;;
  esac
done

# Get list of things to download if not given
if [ -z "${TAGS}" ] ; then read -p "Enter version tag(s): " -a TAGS ; fi

# Create folder if needed
if [ ! -d ${TARGET_FOLDER} ] ; then
  echo "Creating ${TARGET_FOLDER}"
  mkdir -p ${TARGET_FOLDER}
fi

for TAG in ${TAGS[@]} ; do
{
  # Pull artifacts into folder
  echo "Syncing from S3 ${S3_BUCKET_URL}/${TAG}"
  aws s3 sync ${S3_BUCKET_URL}/${TAG} ${TARGET_FOLDER}/${TAG}
  
  # Zip contents for delivery
  echo "Zipping contents to ${TARGET_FOLDER}/${TAG}.tar.gz"
  tar czvf ${TARGET_FOLDER}/${TAG}.tar.gz -C ${TARGET_FOLDER} ${TAG}

## Used if we need to break things up at this stage, but it can probably be done at the upload phase
#  if (( $(tar -cf - ${TARGET_FOLDER}/${TAG} | wc -c) < 6*1024*1024*1024 )); then
#    tar czvf ${TARGET_FOLDER}/${TAG}.tar.gz -C ${TARGET_FOLDER} ${TAG}
#  else
#    tar czvf - ${TARGET_FOLDER}/${TAG} | split -b 6G - ${TARGET_FOLDER}/${TAG}-.tar.gz
#  fi
} &  # Run each loop in the background as a separate process
done 

wait # Wait for background processes to exit (for tracking elapsed time; optional)
