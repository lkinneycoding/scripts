#!/bin/bash
### The whole point of this file is because the time gets out of sync when 
### the laptop is hibernating and doesn't automatically sync up on wake...

# Path to the file
file_path="/var/lock/time_synced"

# Get the last modified time of the file in epoch format
last_modified=$(stat -c %Y "$file_path" 2>/dev/null)

# Get today's date in epoch format
today=$(date -d "today 00:00" +%s)

# Check if the file exists and if its last modified time isn't today
if [ ! -f "$file_path" ] || [ "$last_modified" -lt "$today" ]; then
    # Sync time
    sudo hwclock -s --verbose
    touch $file_path
fi
