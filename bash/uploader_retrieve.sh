#!/bin/bash

### Description: Retrieves files pushed through the API tagged with PREFIX to DEST folder.

PREFIX="uniqueprefix_"     # PREFIX should match what you're sending up with the uploader.py script
DEST="/opt/downloads/"     # Local directory to put all your junk

usage() {
  echo "USAGE: $0 [-p PREFIX] [-d DESTINATION]"
  echo "OPTIONS:"
  echo "  [-p|--prefix]         Set custom file prefix"
  echo "  [-d|--destination]    Set custom file destination"
  echo "  [-h|--help]           Usage message"
  exit 0
}

# Display usage text
if [ $# -eq 0 ] || [[ " $* " == *" -h "* ]] || [[ " $* " == *" --help "* ]]; then
  usage
fi

# Process arguments
while [[ $# -gt 0 ]]; do
  case $1 in
    --prefix|-p)
      PREFIX="$2"
      shift 2
      ;;
    --destination|-d)
      DEST="$2"
      shift 2
      ;;
    -h|--help|*)
      usage
      ;;
  esac
done

# Create DEST if it doesn't exist
if [ ! -d ${DEST} ] ; then mkdir -p ${DEST} ; fi

for FILE in $(ls -1 ~/${PREFIX}* | xargs -n 1 basename) ; do
    mv ~/${FILE} ${DEST}                       # Move file tagged with PREFIX to DEST
    rename -- "${PREFIX}" "" ${DEST}/${FILE}   # Remove PREFIX from filename once in DEST
done
