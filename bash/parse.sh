#!/bin/bash

# Define the array of characters
characters=("{" "}" "(" ")" "\"" "'" "\[" "\]")

# Initialize a line counter
line_number=0

# Loop through each line in the CSV file
while IFS= read -r line; do
    # Increment the line number
    ((line_number++))

    # Initialize a string to store counts for each character
    count_output="Line $line_number: "

    # Loop through each character in the array
    for char in "${characters[@]}"; do
        # Count the occurrences of the character in the line
        count=$(echo "$line" | grep -o "$char" | wc -l)

        # Check if the count is odd
        if (( count % 2 != 0 )); then
            # If odd, colorize the count in red
            count_output+="$char: \e[31m$count\e[0m, "
        else
            # If even, leave the count in default color
            count_output+="$char: $count, "
        fi
    done

    # Remove the trailing comma and space, and print the result
    echo -e "${count_output%, }"

done < $1 # First argument is the filename to pass into the loop
