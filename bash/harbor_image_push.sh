#!/bin/bash

### Description: Push from the local Docker image repo to Harbor
### Prereqs: Make sure you are in the 'docker' group; i.e. `sudo usermod -aG docker <username>` and open a new session

DEST_REPO="registry.example.com"       # Destination Harbor repository
DEST_PROJECT="myproject"               # Project within the destination repo
IMAGE_FILE="${HOME}/image_list.txt"    # Default image list (from Git)
IMAGES=()                              # Initialize image list

# Process argument overrides
while [[ $# -gt 0 ]]; do 
  case $1 in
    --repo|-r)
      DEST_REPO="$2"
      shift 2
      ;;
    --project|-p)
      DEST_PROJECT="$2"
      shift 2
      ;;
    --file|-f)
      IMAGE_FILE="$2"
      shift 2
      ;;
    *) IMAGES+=("$1")
      shift
      ;;
  esac
done

# Login to the destination Harbor
docker login ${DEST_REPO}

# Process image list, if applicable
for image in ${IMAGE_FILE}, ${IMAGES[@]}; do
  echo "Pushing ${image} to ${DEST_REPO}/${DEST_PROJECT}"
  docker push ${image} ${DEST_REPO}/${DEST_PROJECT}/${image}
done
