# BASH Code Repository

For code that makes me want to BASH my head against the desk.

# Descriptions

## get_minio.sh
I needed to download MinIO to simulate S3 conectivity on my local machine.  I think this is pulled straight from their website

## applogger.sh
While deploying my apps on EKS, this brings up logs for the containers that are running.  You can specify which apps you want to monitor, otherwise it will check all of them together.  Each deployment only has one container, but there might be room for improvement if there are multiple containers per deployment.

## tg_healhtchecks.sh
While deploying my apps on EKS, this script checks the K8s Target Group health check statuses.  (e.g. my app is named 'dev3', so I run `./tg_healthchecks.sh dev3` and the TGs report back their status.  The script has additional comments within.

## wsl_timesync.sh
For some inexplicable reason, my work laptop (Windows 11 w/ WSL) doesn't automatically adjust time after it has been hibernating for awhile, and will often be a few minutes out of sync after just a few hours.  This affects my ability to authenticate with AWS and other services, so this script is used to sync the time when I open a WSL terminal if it hasn't been done yet today.

## kube_resources.sh
I was given a bunch of namespaces in our EKS cluster and I just wanted to see what was in them, so this handy script helped me get a quick at-a-glance look at the namespace resources.
