#!/bin/bash

# Process each line of the file
awk '
{
    # Count leading spaces
    n = match($0, /^[ ]*/);
    count = RLENGTH;

    # Check if the first non-whitespace character is not a "#"
    if (substr($0, count+1, 1) != "#" && count % 2 != 0) {
        # Print line number and count of leading spaces if the count is odd
        print NR ": " count
    }
}' $1

