#!/bin/bash

GIT_HOME="${HOME}/git"
GIT_EXCLUDES=("Terraform" "tetris" "sandbox")
GIT_EXCLUDE_SNIPPET=()
for word in "${GIT_EXCLUDES[@]}"; do 
  GIT_EXCLUDE_SNIPPET+="-not -path \"*${word}*\" "
done
FIND_REPO_CMD="find ${GIT_HOME} -name '.git' -type d ${GIT_EXCLUDE_SNIPPET} -exec dirname {} \;"
GIT_REPOS=$(eval ${FIND_REPO_CMD})

for repo in ${GIT_REPOS[@]}; do
  echo "----------"
  echo ${repo}
  echo "----------"
  cd ${repo}
  git pull
done
