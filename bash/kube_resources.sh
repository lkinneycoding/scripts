#!/bin/bash

### Description: Very quick script for getting a decent overview of what's going on in a bunch of predefined namespaces

namespaces=(dev1 test1 prod)

for namespace in ${namespaces[@]}; do
  echo "Namespace: $namespace"
  kubectl get all,cm,secret,ing,pv,pvc -n $namespace
  echo "====="
done
