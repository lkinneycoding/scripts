#!/bin/bash

### Description: After deleting your EKS cluster, it doesn't clean up Persistent Volume Claims, so this script will do that for you
### NOTE: Search can be your EKS cluster name or any other part of the volume name, if you know it.

#SEARCHTERM="logan-eks"
read -p "Search for volumes with (case-sensitive) names containing: " SEARCHTERM

# Query AWS for your volumes
output_json=$(aws ec2 describe-volumes --query "Volumes[?Tags[?Key=='Name' && contains(Value, '${SEARCHTERM}')]].{VolumeId:VolumeId, Name:Tags[?Key=='Name']|[0].Value}" --output json)
if [[ "$output_json" == "[]" ]]; then
  echo "No volumes found matching the criteria: ${SEARCHTERM}"
  exit 0
fi

# Output result to table
echo "$output_json" | jq -r '. | (.[0] | keys_unsorted) as $keys | ($keys, map([.[ $keys[] ]])[]) | @tsv' | column -t

# Save VolumeId to an array
volume_ids=($(echo "$output_json" | jq -r '.[].VolumeId'))

# Confirm volumes to delete
read -p "Enter VolumeIds to delete, or type 'all' to delete them all: " selection

# Remove uppercases for people trying to be cute
selection_lower=$(echo "$selection" | tr '[:upper:]' '[:lower:]')
if [[ ${selection_lower} =~ 'all' ]] ; then
  # Delete all volumes in the original search results
  vols_to_delete=${volume_ids[@]}
else
  # Use the selected VolumeIds only
  vols_to_delete=${selection_lower}
fi
echo "Deleting selected volumes: ${vols_to_delete[@]}"

# Delete selected volumes
for volume in ${vols_to_delete[@]}; do
  echo "Deleting $volume"
  aws ec2 delete-volume --volume-id $volume
done
