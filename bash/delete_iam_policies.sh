#!/bin/bash

CLUSTER_NAME="myekscluster"
policy_arns=$(aws iam list-policies --query "Policies[?contains(PolicyName, '${CLUSTER_NAME}')].Arn" --output text)

for policy_arn in $policy_arns; do
  echo "Processing policy $policy_arn"

  # Detach the policy from all users
  user_attachments=$(aws iam list-entities-for-policy --policy-arn $policy_arn --query "PolicyUsers[].UserName" --output text)
  for user in $user_attachments; do
    echo "Detaching policy $policy_arn from user $user"
    aws iam detach-user-policy --user-name $user --policy-arn $policy_arn
  done

  # Detach the policy from all roles
  role_attachments=$(aws iam list-entities-for-policy --policy-arn $policy_arn --query "PolicyRoles[].RoleName" --output text)
  for role in $role_attachments; do
    echo "Detaching policy $policy_arn from role $role"
    aws iam detach-role-policy --role-name $role --policy-arn $policy_arn
  done

  # Detach the policy from all groups
  group_attachments=$(aws iam list-entities-for-policy --policy-arn $policy_arn --query "PolicyGroups[].GroupName" --output text)
  for group in $group_attachments; do
    echo "Detaching policy $policy_arn from group $group"
    aws iam detach-group-policy --group-name $group --policy-arn $policy_arn
  done

  # Delete the policy
  echo "Deleting policy $policy_arn"
  aws iam delete-policy --policy-arn $policy_arn


done

