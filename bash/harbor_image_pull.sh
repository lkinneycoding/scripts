#!/bin/bash

### Description: Pull from the local Harbor to push to the remote Harbor
### Prereqs: Make sure you are in the 'docker' group; i.e. `sudo usermod -aG docker <username>` and open new terminal

# Default values
SOURCE_REPO="harbor.example.com"
IMAGE_LIST="${HOME}/images.txt"
DEST_FOLDER="${HOME}/s3-downloads"

# Help text
usage() {
  echo "USAGE: $0 -l /path/to/images.txt [OPTIONS]"
  echo "OPTIONS:"
  echo "  [-s|--source]          Source repository"
  echo "  [-l|--list]            File containing list of images"
  echo "  [-d|--destination]     Local path to store images"
  echo "  [--clean]              Delete untagged images after download"
  echo "  [-h|--help]            Usage message"
  exit 0
}

# Parse arguments
while [[ $# -gt 0 ]]; do
  case $1 in
    --source|-s)
      SOURCE_REPO="$2"
      shift 2
      ;;
    --list|-l)
      IMAGE_LIST="$2"
      shift 2
      ;;
    --destination|-d)
      DEST_FOLDER="$2"
      shift 2
      ;;
    --clean)
      delete_old_images="true"
      shift 1
      ;;
    --help|-h)
      usage
      ;;
    *) echo "Using default values"
      ;;
  esac
done

# Create destination folder if needed
if [ ! -d ${DEST_FOLDER} ] ; then
  echo "Creating ${DEST_FOLDER}"
  mkdir -p ${DEST_FOLDER}
fi

# Debut text
echo "Pulling images listed in ${IMAGE_LIST} from ${SOURCE_REPO} to ${DEST_FOLDER}"

# Login to the source Harbor
docker login ${SOURCE_REPO}

# Loop through each image in the list
for image in $(cat ${IMAGE_LIST}); do
{
    image_name=$(echo ${image} | awk -F '/' '{gsub(/:/, "-",$NF); print $NF}')  # Isolate [image]:[tag]
    dest_path=$(echo ${DEST_FOLDER}/${image_name}.tar)                          # Define destination archive
    echo "Pulling ${image}"
    docker pull ${SOURCE_REPO}/${image}                                         # Retrieve image from source
    echo "Saving ${image} to ${dest_path}"
    docker save ${SOURCE_REPO}/${image} -o ${dest_path}                         # Save image to local disk for transport
} & # Push each loop to the background
done

# Wait for all loops to complete
wait

# Clean up untagged images
if [[ "${delete_old_images}" == "true" ]]; then
  echo "Removing untagged images"
  docker image prune --force
fi
