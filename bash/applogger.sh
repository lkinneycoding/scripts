#!/bin/bash 

### Description: Uses TMUX to tail the logs of apps in the given namespace

session_name="applogger"                                    # Name of the tmux session
create_session=$(tmux list-sessions | grep $session_name)   # Check if a session by that name needs to be created
namespace="$1"                                              # First arg is the cluster namespace
shift                                                       # Drop the namespace arg after capture
apps="$@"                                                   # All other args are the apps you want to monitor
if [ -z "${apps}" ] ; then                                  # If you don't specify any apps, it does them all
  apps="core jobs log-exporter messenger metrics nginx scheduler search"
fi

# If tmux isn't installed, exit
if ! which tmux ; then echo "Verify that tmux is installed and in your PATH" ; exit ; fi

# If session doesn't exist, create it
if $create_session ; then
  # Create a new tmux session
  tmux new-session -d -s $session_name
  for app in $apps ; do 
    # Get pod name for this cluster
    pod_name=$(kubectl get pods -n $namespace | grep "^${app}" | awk '{print $1}')
    # Create a pane to run logs for this pod
    tmux split-window -t $session_name: "bash -c 'kubectl logs -fn $namespace $pod_name'"
    # Set name for this pane
    tmux select-pane -T "$app"
    # Balance pane sizes to fit more
    tmux select-layout even-vertical
  done
  # Kills the original pane which isn't doing anything
  tmux kill-pane -t 0
  # Final pane balancing
  tmux select-layout even-vertical
fi

# Attach to the tmux session
tmux attach-session -t $session_name

