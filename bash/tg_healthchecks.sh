#!/bin/bash

### Description: Use to get the status of the health checks for any Target Groups with "search_term" in the name
# I used this to check the status of the individual app components as they were being deployed in EKS

# If no search term is given, get a status check for all target groups in the account
if [[ -z $1 ]] ; then search_term="targetgroup" ; else search_term=$1 ; fi

# List target groups that match the search
target_groups=$(aws elbv2 describe-target-groups --query "TargetGroups[*].TargetGroupArn" --output yaml | grep $search_term | awk '{print $2}')

# Get health check status for each target group and display it
# Blank means no health check is configured
for tg in $target_groups ; do
  tgstat=$(aws elbv2 describe-target-health --target-group-arn $tg --query "TargetHealthDescriptions[].TargetHealth.State" --output text)
  echo -e "${tg}\t${tgstat}"
done
