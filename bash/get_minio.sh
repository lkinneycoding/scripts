#!/bin/bash

curl https://dl.min.io/client/mc/release/linux-amd64/mc \
  --create-dirs \
  -o $HOME/minio-binaries/mc

chmod +x $HOME/minio-binaries/mc
echo 'export PATH=$PATH:$HOME/minio-binaries/' >> $HOME/.bashrc
source $HOME/.bashrc

mc --help
mc --autocompletion
