#!/bin/bash

### Create admin account to manage puppet server

# Function to collect user credentials
collect_credentials() {
    read -p "Enter admin username: " ADMINUSERNAME
    read -s -p "Enter password: " ADMINPASSWORD
    echo
    read -p "Enter user's home directory (default: /home/${ADMINUSERNAME}): " HOMEDIR
    HOMEDIR=${HOMEDIR:-/home/${ADMINUSERNAME}
}

# Function to create a Puppet manifest file with the name username.pp
create_puppet_manifest() {
    manifest_file="${ADMINUSERNAME}.pp"
    cat <<EOF > $manifest_file
user { '${ADMINUSERNAME}':
  ensure     => 'present',
  managehome => true,
  home       => '${HOMEDIR}',
  password   => '`openssl passwd -1 ${ADMINPASSWORD}`',
}
EOF
    echo "Puppet manifest file '$manifest_file' created."
}

# Function to apply the Puppet manifest
apply_puppet_manifest() {
    puppet apply "${ADMINUSERNAME}.pp"
}

# Main script execution
collect_credentials
create_puppet_manifest
#apply_puppet_manifest

