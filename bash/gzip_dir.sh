#!/bin/bash

### Description: Creates a .tar.gz of a directory for shipping up to wherever

usage() {
  echo "USAGE: $0 /path/to/folder [/path/to/folder2] [...]"
  echo "OPTIONS:"
  echo "  [-h|--help] Usage message"
  exit 0
}

# Display usage text
if [ $# -eq 0 ] || [[ " $* " == *" -h "* ]] || [[ " $* " == *" --help "* ]]; then
  usage
fi

DIRS=($@)

for DIR in ${DIRS[@]} ; do
  {
  if [ -d $DIR ] ; then
    # Remove trailing slash ('/') if there is one
    full_path=$(realpath -s ${DIR})
    # Split base name from dir path
    work_dir=$(dirname $full_path)
    base_dir=$(basename $full_path)
    # Change working dir
    cd ${work_dir}
    # Gzip directory in the same path that it lives in
    tar czvf ${base_dir}.tar.gz ${base_dir}
  else
    # Skip if not a directory
    echo "##### ${DIR} is not a directory #####"
  fi
  } & # Run each loop in the background
done

wait # Wait for the loops to finish
