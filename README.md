# Scripts

Repository for scripts I created in various languages, often under the influence of caffeine and lack of sleep.  See subfolder README files for descriptions of the different scripts, what they intend to do, and maybe even some ways they can be improved.

## Bash

The Bash scripts were written in either Ubuntu or CentOS/Rocky environments.  I might name them accordingly, if needed.

## Python

I'm learning Python, so occasionally I'll come across an issue that either works better than Bash or is just a good opportunity to learn a simple task in Python.

## PowerShell

As a Linux guy stuck in a Windows-centric environment, sometimes you have to get your hands dirty and do some PowerShell.  I believe most of the scripts that will end up here are written in PowerShell v5. 
