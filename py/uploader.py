#!/usr/bin/python3

import os
import stat
import requests
import urllib.parse
import argparse
import getpass
import json
import glob
import math
import hashlib
import sys
from datetime import datetime, timedelta

API_URL = "https://uploader.example.com/api"                 # Base URL for the API
API_METHOD_TOKEN = "/login/token"                            # Method for getting auth token
API_METHOD_UPLOAD = "/upload"                                # Method for uploading files
TOKEN_FILE = os.path.expanduser("~/.uploader/token.json")    # Location to save login token
FILE_PREFIX = "uniqueprefix_"                                # Prefix to identify files to be sent to API
MAX_PAYLOAD_SIZE = 6 * 1024 * 1024 * 1024                    # 6GB payload limit, set by API

def get_token():
    # Check if token is already cached and not expired
    token = load_token()
    if token and token_not_expired(token):
        print("Using cached token.")
        return token['access_token']

    # Retrieve username and password
    username = input("Enter your username: ")
    password = getpass.getpass("Enter your password: ")

    # Form components to send to POST
    url = API_URL + API_METHOD_TOKEN
    headers = {'accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
    payload = {'username': username, 'password': password}

    # Handle special characters in the headers
    encoded_payload = urllib.parse.urlencode(payload).encode('utf-8')

    try:
        response = requests.post(url, data=encoded_payload, headers=headers)
        response.raise_for_status()
        token = response.json()
        save_token(token)
        return token['access_token']
    except requests.exceptions.RequestException as e:
        print("Error:", e)
        return None

def load_token():
    # Read saved token data
    try:
        with open(TOKEN_FILE, 'r') as file:
            token_data = json.load(file)
        return token_data
    except (FileNotFoundError, json.JSONDecodeError):
        pass
    return None

def save_token(token):
    # Create directory to save token to, if needed
    os.makedirs(os.path.dirname(TOKEN_FILE), exist_ok=True)

    # Save token JSON data to file
    with open(TOKEN_FILE, 'w') as file:
        json.dump(token, file)

    # Set file permissions to 0600
    os.chmod(TOKEN_FILE, stat.S_IRUSR | stat.S_IWUSR)

def token_not_expired(token):
    # Read the creation time of the token file
    token_ctime = datetime.fromtimestamp(os.path.getctime(TOKEN_FILE))

    # Calculate the expiry time based on the 'expires_in' attribute in the token
    expiry = token_ctime + timedelta(seconds=token['expires_in'])

    # Check if the expiry time is greater than the current time
    return expiry > datetime.now()

def get_human_readable_size(size_bytes):
    # Get human readable size for the file
    if size_bytes == 0:
        return '0B'
    size_name = ('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB')
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return f"{s} {size_name[i]}"

def calculate_md5(file_path):
    # Calculate MD5 hash of the file
    hasher = hashlib.md5()
    with open(file_path, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b''):
            hasher.update(chunk)
    return hasher.hexdigest()

def upload_files(token, file_paths):
    # Used to form the POST request
    url = API_URL + API_METHOD_UPLOAD
    headers = {'Authorization': f'Bearer {token}'}

    files = []
    for file_path in file_paths:
        if os.path.isfile(file_path):
            # Generate new filename
            new_filename = f"{FILE_PREFIX}{os.path.basename(file_path)}"
            # Get human-readable file size
            file_size = os.path.getsize(file_path)
            readable_size = get_human_readable_size(file_size)
            # Skip file if size exceeds MAX_PAYLOAD_SIZE
            if file_size > MAX_PAYLOAD_SIZE:
                print(f"File {file_path} is too large to send... skipping")
                continue
            # Calculate MD5 hash
            md5_hash = calculate_md5(file_path)
            # Display file information
            print(f"File: {new_filename}, Size: {readable_size}, MD5: {md5_hash}")
            # Add file_path to the list of files to be sent
            files.append(('files', (new_filename, open(file_path, 'rb'))))
        else:
            print(f"File not found: {file_path}")

    try:
        response = requests.post(url, headers=headers, files=files)
        response.raise_for_status()  # Raise an exception for bad response status
        print("Files uploaded successfully.")
    except requests.exceptions.RequestException as e:
        print("Error:", e)

def process_files(token, file_paths):
    current_payload = []
    current_size = 0
    
    for file_path in file_paths:
        file_size = os.path.getsize(file_path)
        
        # If adding this file exceeds the max payload size, send the current payload and start a new payload
        if current_size + file_size > MAX_PAYLOAD_SIZE:
            print("##### Sending Partial Payload #####")
            print(f"Payload: {current_payload}")
            print("Payload Size: " + get_human_readable_size(current_size))
            upload_files(token, current_payload)
            current_payload = []
            current_size = 0

        # Add file to payload
        current_payload.append(file_path)
        current_size += file_size

    # Send remaining files
    print("##### Sending Payload #####")
    print(f"Payload: {current_payload}")
    print("Payload Size: " + get_human_readable_size(current_size))
    upload_files(token, current_payload)
       
def main():
    parser = argparse.ArgumentParser(description="Upload files to API.")
    parser.add_argument("files", nargs='+', help="List of files to upload or a glob pattern")
    args = parser.parse_args()

    file_paths = []
    for file_path in args.files:
         file_paths.extend(glob.glob(file_path))

    token = get_token()
    if token:
        process_files(token, file_paths)
    else:
        print("Login failed.")

if __name__ == "__main__":
    main()

