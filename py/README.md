# Python Code Repository

Just a space to keep my Python code

# Descriptions

## python-ascii

This was one of my first attempts at writing a Python app.  I needed something to display on my Docker web page and show that the app was working, so I made it convert an image file to ASCII text and display that at the container's URL.  There are a few different types of files of varying resolutions to test how it displayed in the browser.

## uploader.py

I have a use case for pushing files to an external server that is disconnected from direct access to my local machine.  The uploader API is the best way to get larger files (like custom JAR files or Docker images) into the environment.

* The script authenticates with the remote server and sends whatever files you tell it to push.
* The API has a 6GB per POST request size limit, so this was incorporated in the logic to break out your files into chunks that fit within that spec.
* It also adds a prefix to the file name since the remote server just dumps things straight into my home directory and I wanted a way to easily find files I'm sending up.
* There's another script on the other side to watch my home directory for this prefix and rename the files once it pushes them to their final destination on the server.

## post-patching.py

Once upon a time, I was tasked with making sure our developers rebooted their machines after Patch Day, everyone's favorite time of the month.  Easy enough to do it ourselves, but we didn't want to interrupt anyone's work, and some developers have scripts running overnight/weekends, so if they requested that we not reboot their machines, we'd historically just tell them to take care of it themselves some time after patch day.  Well, no one would do it on their own and some machines would go months, if not years, without a single reboot (yes, these were Linux machines, of course).

We couldn't wait forever.  Long(er) story short, the ISSMs eventually said enough is enough.  Give them 10 days to reboot their machines, or do it for them.  Fine by me, and while I could just make a script to tell VMware to pull the rug out from under them 10 days after patch day, that didn't seem sophisticated enough and I'd rather not have some central script go and do mass reboots everywhere.  So, I devised a system that works like this:

* Everyone is aware of the 10 day post-patching reboot policy.
* Patching was done with an Ansible playbook, which would create a blank file at /var/lock/system_patched once it completed patching the remote machine.  Splunk monitors for this file to track which machines are patched, and can track the mtime of the file to see how long it's been since the patching occurred.
* There would also be a line added to /etc/motd (Message of the Day) with a patch status for any user logging into the host (either via UI or SSH) to see and take note of.
* If a user or group of users requested an extension, they would have to get an ISSM to bless the extension on each individual machine and request that we block their machine from rebooting during the current patch cycle only.  Each waiver was only good for the current cycle, so if they needed to skip rebooting again during the next cycle, they would have to request another extension.  Some of the users' jobs would be running for weeks at a time, and sometimes Patch Day falls somewhere in the middle.  So, there was a legitimate need to be able to stop the reboots from happening, if needed.  Splunk also knew which machines had the waiver, which is just a blank /var/lock/issm_reboot_waiver file.
* This post-patching.py script, run via root's crontab, checks periodically for the status of these files and will take different actions depending on the age of the lock file and whether or not that 
particular system has an ISSM reboot waiver.  Since it should be a very short list of hosts with the waiver, the idea was to have a dashboard listing the hosts that have this file, hosts that should have this file (imported from somewhere else like a JIRA ticket), and how long since that host has been rebooted.
* During the 10 days, the MOTD file would be updated to reflect that patching occurred and a reboot is required.  If the ISSM waiver is in place, it says that too.
* After 10 days (or some number of days configured in the script), the mtime of the system_patched file would trigger `/sbin/reboot` if the waiver file is absent.
* Upon rebooting, regardless of how long it's been, these /var/lock files will be removed and there is no need to go clean them up later.  The next time the script runs from the cron, it also has a section to remove the patching status from the MOTD file since it's no longer needed at that point.