#!/bin/python3

import os
import time
import subprocess

lock_file_path = '/var/lock/system_patched'
reboot_waiver_file_path = '/var/lock/issm_reboot_waiver'
motd_file_path = '/etc/motd'
max_days = 10
max_age = max_days * 24 * 60 * 60  # max_days in seconds
current_time = time.time()

# Check if lock file exists
if os.path.exists(lock_file_path):
    file_age = current_time - os.path.getmtime(lock_file_path)

    # Check if lock file is over max_days old and reboot waiver file doesn't exist
    if file_age > max_age and not os.path.exists(reboot_waiver_file_path):

        # Log to message stream
        print(f"Lock file is over {max_days} days old. Initiating reboot.")
        logger_command = ['/bin/logger', f'EVENT=days_since_patching STATUS=rebooted_after_{max_days}_days']
        subprocess.run(logger_command)

        # Update /etc/motd
        with open(motd_file_path, 'r+') as motd_file:
            lines = motd_file.readlines()
            motd_file.seek(0)
            for line in lines:
                if not line.startswith('PatchingStatus='):
                  motd_file.write(line)
            status_line = 'PatchingStatus=Rebooted machine after {} days post-patching\n'.format(max_days)
            motd_file.write(status_line)
            motd_file.truncate()

        # Reboot
        subprocess.run(['/sbin/reboot'])

    # Check if lock file is over max_days days old and reboot waiver file exists
    elif file_age > max_age and os.path.exists(reboot_waiver_file_path):

        # Log to message stream
        print(f"Lock file is over {max_days} days old, but reboot waiver is in place.")
        logger_command = ['/bin/logger', 'EVENT=days_since_patching STATUS=ISSM_reboot_waiver_in_place']
        subprocess.run(logger_command)

        # Update /etc/motd
        with open(motd_file_path, 'r+') as motd_file:
            lines = motd_file.readlines()
            motd_file.seek(0)
            for line in lines:
                if not line.startswith('PatchingStatus='):
                  motd_file.write(line)
            status_line = 'PatchingStatus=ISSM reboot waiver in place\n'
            motd_file.write(status_line)
            motd_file.truncate()

    # Check if lock file is within max_days old
    elif file_age <= max_age:

        # Log to message stream
        print(f"Lock file is within {max_days} days old.")
        logger_command = ['/bin/logger', f'EVENT=days_since_patching STATUS=reboot_required_within_{max_days}_days']
        subprocess.run(logger_command)

        # Update /etc/motd
        with open(motd_file_path, 'r+') as motd_file:
            lines = motd_file.readlines()
            motd_file.seek(0)
            for line in lines:
                if not line.startswith('PatchingStatus='):
                  motd_file.write(line)
            status_line = 'PatchingStatus=Reboot required within {} days post-patching.\n'.format(max_days)
            motd_file.write(status_line)
            motd_file.truncate()


    else:
        # Remove PatchingStatus line from /etc/motd
        with open(motd_file_path, 'r+') as motd_file:
            lines = motd_file.readlines()
            motd_file.seek(0)
            for line in lines:
                if not line.startswith('PatchingStatus='):
                  motd_file.write(line)
            motd_file.truncate()
