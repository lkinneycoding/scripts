#!/usr/bin/env python

import os
from PIL import Image
import argparse

def image_to_ascii(image_path):
    # Load the image
    img = Image.open(image_path)

    # Calculate the aspect ratio to maintain the original image's proportions
    aspect_ratio = img.height / (2 * img.width)
    output_width = 150
    output_height = int(output_width * aspect_ratio)

    # Resize the image while maintaining aspect ratio
    img = img.resize((output_width, output_height))

    # Define a string of ASCII characters from dark to light
    ascii_chars = "@%#*+=-:,. "

    # Initialize the ASCII string
    ascii_str = ""

    # Iterate through the pixels and convert to ASCII
    for y in range(output_height):
       for x in range(output_width):
           pixel = img.getpixel((x, y))
           grayscale_value = sum(pixel) // 3
           ascii_str += ascii_chars[grayscale_value * len(ascii_chars) // 256]
       ascii_str += "\n"  # Add a newline after each row

    return ascii_str

# Example usage
image_path = "mickey.jpg"
output_ascii = image_to_ascii(image_path)

# Print or save the output ASCII text
print(output_ascii)

# Save to file
with open('output.txt', 'w') as file:
    file.write(output_ascii)
