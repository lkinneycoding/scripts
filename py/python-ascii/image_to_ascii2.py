#!/usr/bin/env python

import os
from PIL import Image
import argparse

def image_to_ascii(image_path):
    # Load the image
    img = Image.open(image_path)

    # Calculate the aspect ratio to maintain the original image's proportions
    aspect_ratio = img.height / (2 * img.width)
    #aspect_ratio = img.height / img.width
    output_width = 150
    output_height = int(output_width * aspect_ratio)

 #   if img.width > img.height:
 #       aspect_ratio = img.width / img.height
 #       output_width = 300
 #       output_height = int(output_width / aspect_ratio)
 #   else:
 #       aspect_ratio = img.height / img.width
 #       output_width = 200
 #       output_height = int(output_width * aspect_ratio)

    # Resize the image while maintaining aspect ratio
    img = img.resize((output_width, output_height))

    # Define a string of ASCII characters from dark to light
    ascii_chars = "@%#*+=-:,. "
    #ascii_chars = "█▇▆▅▄▃▂▁,. "
    #ascii_chars = "$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\|()1[]?-_+~<>i!lI;:,^`'. "

    # Initialize the ASCII string
    ascii_str = ""

    # Iterate through the pixels and convert to ASCII
    for y in range(output_height):
       for x in range(output_width):
           pixel = img.getpixel((x, y))
           grayscale_value = sum(pixel) // 3
           ascii_str += ascii_chars[grayscale_value * len(ascii_chars) // 256]
       ascii_str += "\n"  # Add a newline after each row

#    for y in range(0, output_height, 2):  # Process two rows at a time
#         for x in range(output_width):
#             pixel1 = img.getpixel((x, y))
#             pixel2 = img.getpixel((x, y+1))
#             grayscale_value1 = sum(pixel1) // 3
#             grayscale_value2 = sum(pixel2) // 3
#             avg_grayscale = (grayscale_value1 + grayscale_value2) // 2
#             ascii_str += ascii_chars[avg_grayscale * len(ascii_chars) // 256]
#         ascii_str += "\n"
 
    return ascii_str

def process_image(image_path):
    output_file = os.path.splitext(os.path.basename(image_path))[0] + ".txt"
    output_path = os.path.join("output", output_file)

    output_ascii = image_to_ascii(image_path)

    # Print to screen
    print(output_ascii)

    # Save to file
    with open(output_path, 'w') as file:
        file.write(output_ascii)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert images to ASCII art.")
    parser.add_argument("image_path", type=str, help="Path to an image file or directory containing images")

    args = parser.parse_args()

    input_path = args.image_path

    if os.path.isfile(input_path) and input_path.lower().endswith(('.png', '.jpg', '.jpeg')):
        process_image(input_path)
    elif os.path.isdir(input_path):
        os.makedirs("output", exist_ok=True)  # Create output directory if it doesn't exist
        for filename in os.listdir(input_path):
            if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
                image_file = os.path.join(input_path, filename)
                process_image(image_file)
    else:
        print(f"Error: {input_path} is not a valid file or directory.")